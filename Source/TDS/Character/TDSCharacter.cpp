// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "GameFramework/GameSession.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(CameraBoom);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	CheckSprintDirection();
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
	
	PlayerInputComponent->BindAction(TEXT("AimState"), IE_Pressed, this, &ATDSCharacter::SetAimMovementState);
	PlayerInputComponent->BindAction(TEXT("AimState"), IE_Released, this, &ATDSCharacter::ResetAimMovementState);
	PlayerInputComponent->BindAction(TEXT("WalkState"), IE_Pressed, this, &ATDSCharacter::SetWalkMovementState);
	PlayerInputComponent->BindAction(TEXT("WalkState"), IE_Released, this, &ATDSCharacter::ResetWalkMovementState);
	PlayerInputComponent->BindAction(TEXT("SprintState"), IE_Pressed, this, &ATDSCharacter::SetSprintMovementState);
	PlayerInputComponent->BindAction(TEXT("SprintState"), IE_Released, this, &ATDSCharacter::ResetSprintMovementState);
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f,0.0f,0.0f), AxisX);
	AddMovementInput(FVector(0.0f,1.0f,0.0f), AxisY);
	
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult ResultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);

		float FindRotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResult , 0.0f)));
	}
}

void ATDSCharacter::CharacterUpdate()
{
	static float ResultSpeed;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResultSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Run_State:
		ResultSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Walk_State:
		ResultSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Sprint_State:
		ResultSpeed = MovementInfo.SprintState;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResultSpeed;
}

void ATDSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	if (MovementState == EMovementState::Sprint_State && NewMovementState == EMovementState::Aim_State)
	{
		PrevMovementState = EMovementState::Sprint_State;
		MovementState = EMovementState::Aim_State;
	}
	else if (MovementState == EMovementState::Walk_State && NewMovementState == EMovementState::Aim_State)
	{
		PrevMovementState = EMovementState::Walk_State;
		MovementState = EMovementState::Aim_State;
	}
	
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATDSCharacter::SetAimMovementState()
{
	ChangeMovementState(EMovementState::Aim_State);
}

void ATDSCharacter::ResetAimMovementState()
{
	if (PrevMovementState == EMovementState::Sprint_State)
	{
		ChangeMovementState(EMovementState::Sprint_State);
		PrevMovementState = EMovementState::Run_State;
	}
	else if (PrevMovementState == EMovementState::Walk_State)
	{
		ChangeMovementState(EMovementState::Walk_State);
		PrevMovementState = EMovementState::Run_State;
	}
	else
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

void ATDSCharacter::SetWalkMovementState()
{
	if (MovementState == EMovementState::Aim_State)
	{}
	else
	{
		ChangeMovementState(EMovementState::Walk_State);
	}
}

void ATDSCharacter::ResetWalkMovementState()
{
	if(MovementState == EMovementState::Aim_State)
	{
		PrevMovementState = EMovementState::Run_State;
	}
	else
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}


void ATDSCharacter::SetSprintMovementState()
{
	if (AngleInDegree < MaxSprintAngle)
	{
		ChangeMovementState(EMovementState::Sprint_State);
	}
}

void ATDSCharacter::ResetSprintMovementState()
{
	if(MovementState == EMovementState::Aim_State)
	{
		PrevMovementState = EMovementState::Run_State;
	}
	else
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

void ATDSCharacter::CheckSprintDirection()
{
	float DotProduct = FVector::DotProduct(GetVelocity().GetSafeNormal(), GetActorForwardVector().GetSafeNormal());
	AngleInDegree = FMath::Acos(DotProduct);
	AngleInDegree = FMath::RadiansToDegrees(AngleInDegree);
	if (MovementState == EMovementState::Sprint_State && AngleInDegree > MaxSprintAngle)
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}
